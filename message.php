<?php
require "vendor/autoload.php";
session_start();

date_default_timezone_set('UTC');
$result = [];
if (!isset($_SESSION['gaccess']) || !isset($_GET['id'])) {
    $result['error'] = 'didn\'t login';
}else {

    $google = new Google($clientId, $clientSecret);
    $client = $google->getClient();
    $accessToken = $_SESSION['gaccess'];
    $client->setAccessToken($accessToken);

    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    }

    $service = new Google_Service_Gmail($client);

    $threads = array();
    $limit = 0;
    $pageToken = NULL;
    $opt_param = array();

    /** @var Google_Service_Gmail_Message $response */
    $response = $service->users_messages->get('me', $_GET['id']);

    if($response->getPayload()->getBody()->size == 0){
        /** @var Google_Service_Gmail_MessagePart $part */
        foreach ($response->getPayload()->getParts() as $part){
            foreach($part->getHeaders() as $header){

                if($header->name == 'Content-Type' && substr_count($header->value,'text/html')){
                    $base64Body = $part->getBody()->getData();
                    break 1;
                }
            }
        }
    } else {
        $base64Body = $response->getPayload()->getBody();
    }

    foreach ($response->getPayload()->getHeaders() as $header) {
        switch ($header->name) {
            case 'Subject':
            case 'To':
            case 'From':
            $result[] = ['name' => $header->name, 'value' => $header->value];
        }
    }
    $result[] = ['name' => 'ID', 'value' => $response->getId()];
}

$t = json_encode([
    'message' => [
        'body' => $base64Body,
        'attrs' => $result
    ]
]);
//echo '<pre>';
//var_dump($result);
echo $t;

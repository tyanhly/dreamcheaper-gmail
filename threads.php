<?php
require "vendor/autoload.php";
session_start();

date_default_timezone_set('UTC');
$result = [];
if (!isset($_SESSION['gaccess'])) {
    $result['error'] = 'didn\'t login';die;
}

$google = new Google($clientId, $clientSecret);
$client = $google->getClient();
$accessToken = $_SESSION['gaccess'];
$client->setAccessToken($accessToken);

if ($client->isAccessTokenExpired()) {
    $_SESSION['gaccess'] = $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
}

$service = new Google_Service_Gmail($client);

$threads = array();
$limit = 0;
$pageToken = NULL;
$opt_param = array();

do {
    try {
        $opt_param = array();
        if ($pageToken) {
            $opt_param['pageToken'] = $pageToken;
        }
        $threadsResponse = $service->users_threads->listUsersThreads('me', $opt_param);
        if ($threadsResponse->getThreads()) {
            $threads = array_merge($threads, $threadsResponse->getThreads());
            $pageToken = $threadsResponse->getNextPageToken();
//            echo count($threads);
        }
    } catch (Exception $e) {
        print 'An error occurred: ' . $e->getMessage();
        $pageToken = NULL;
    }
} while ($pageToken && $limit++ <5);

echo json_encode(['threads' => $threads]);
//var_dump($list);

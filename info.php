<?php
require "vendor/autoload.php";
session_start();

date_default_timezone_set('UTC');
if (!isset($_SESSION['gaccess'])) {
    $result['error'] = 'didn\'t login';die;
}

$accessToken = $_SESSION['gaccess'];
$datetime = new \DateTime();
$datetime->setTimestamp($accessToken['created']);
$datetime->setTimeZone(new DateTimeZone('Asia/Ho_Chi_Minh'));
$accessToken['createdDate'] = $datetime->format('Y-m-d H:i:s');

echo json_encode($accessToken);

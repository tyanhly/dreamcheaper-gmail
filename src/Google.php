<?php


class Google
{

    private $client;

    /**
     * Google constructor.
     * @param $clientId
     * @param $clientSecret
     */
    public function __construct($clientId, $clientSecret)
    {
        $this->client = new \Google_Client();
        $this->client->setApplicationName("DreamCheaper");
        $this->client->setClientSecret($clientSecret);
        $this->client->setClientId($clientId);
        $this->client->setAccessType("offline");
        $this->client->setRedirectUri("http://localhost:9999");
        $this->client->setIncludeGrantedScopes(true);
        $this->client->setScopes(array(
            \Google_Service_Gmail::GMAIL_READONLY
        ));
    }

    /**
     * @return Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Google_Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

}
